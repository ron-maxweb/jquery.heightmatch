# jQuery.heightMatch
Use a jQuery plugin to make 2 or elements use the same height.

## Usage
In your HTML, add any class to your element, `height-match` for example.
```html
<div class="my-larger-element height-match">
	Large box
</div>
<div class="my-smaller-element height-match">
	Small box
</div>
```

Load up the jQuery plugin.
```html
<script type="text/javascript" src="/js/jquery.heightmatch.js"></script>
```

Call the `$.heightMatch` function.
```javascript
$('.height-match').heightMatch();
```

## Notes
 - You can use any applicable selector, but to keep good standards it's best to use a CSS class.
 - You can use this plugin multiple times on the same page, just use a different selector/class.