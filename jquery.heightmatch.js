/**
 * $.fn.heightMatch()
 * @author Ron Marsden <ron.marsden@maxwebsolutions.co.uk>
 * @version 1.0
 *
 * Loops through all items with a specific class name and applies the same height to them all.
 * USAGE
 * Add the CSS class 'height-match' to the elements you want to match heights on.
 * Then add the following to your JavaScript: $('.height-match').heightMatch();
 * 
 */
$.fn.heightMatch = function() {
    var allHeights = [];

    // Loop through all the items and find their heights, adding them to an array.
    $.each(this, function(k, v) {
        allHeights.push($(v).height());
    });

    // Sort the array of heights.
    allHeights.sort();

    // Set every element to have the same height as the tallest element.
    $.each(this, function(k, v) {
        $(v).height(allHeights[0]);
    });
}